**Simulation d'évolution d'un feu de fôret représenter sur une interface Tkinter**

Langage utilisé: Python

exemple de lancement du script sans/avec paramètres (ligne colonne duree taux_boisement) : 

- py -3 index.py
- py -3 index.py ligne=20 colonne=20 duree=15 forestation=0.7
- py -3 index.py duree=15 colonne=20  forestation=0.7 ligne=20    

    -> l'ordre importe peu, le plus important est le nommage du paramètre spécifié.
    -> l'utilisateur peut spécifier autant de paramètre qu'il le souhaite, les autres seront initialisé par défaut.


Groupe 2:
    * Léo Tavernier
    * Samy Mokhtari