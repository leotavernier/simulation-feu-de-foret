import csv
import os

entetes = [
      u'ligne',
      u'colonne',
      u'duree',
      u'forestation'
]

# Créer le fichier et écrit les données sous forme de dictionnaire (potentiellement mise à jour via les paramètres de script par l'user) dans le fichier .csv 
def creation_fichier(chemin,config):
   with open(chemin, 'w', newline='') as csvfile:
      writer = csv.DictWriter(csvfile, fieldnames=entetes)
      writer.writeheader()
      writer.writerow({'ligne': config['ligne'], 'colonne': config['colonne'], 'duree': config['duree'] ,'forestation':config['forestation'] })

# Lit le fichier csv et renvoie une liste contenant les paramètres
def lecture_fichier(chemin,configuration):

   configuration=dict()
   if os.path.exists(chemin) and os.path.isfile(chemin):
      with open(chemin, newline='') as csvfile:
         reader = csv.DictReader(csvfile)
         try:
            if(type(configuration)==list):
               lecture_liste(configuration,reader)
            else:
               lecture_dict(configuration,reader)
         except Exception:
            print('Oups... Une erreur de lecture sur le fichier de données à été trouvée.', Exception)

   return configuration

def lecture_liste(liste,reader):
   for row in reader:
      liste[0]=int(row['ligne'])
      liste[1]=int(row['colonne'])
      liste[2]=float(row['duree'])
      liste[3]=float(row['forestation'])


def lecture_dict(configuration,reader):
   for row in reader:
      configuration['ligne']=int(row['ligne'])
      configuration['colonne']=int(row['colonne'])
      configuration['duree']=float(row['duree'])
      configuration['forestation']=float(row['forestation'])


def ecriture_fichier(chemin,liste):
   with open(chemin, 'w', newline='') as csvfile:
      writer = csv.DictWriter(csvfile, fieldnames=entetes)
      writer.writeheader()
      writer.writerow({'ligne': liste[0], 'colonne': liste[1], 'duree': liste[2],'forestation': liste[3] })