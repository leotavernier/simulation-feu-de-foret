import sys

# IN: Arguments passes au script
# OUT: Liste comprenant les arguments mis à jours
def MAJ_Arguments(config,args):
    try:
        l='ligne'
        c='colonne'
        d='duree'
        t='forestation'
        for arg in args:
            
            k=arg.split('=')[0]
            v=arg.split('=')[1]
            if(k==l):
                config[l]=int(v)
            elif(k==c):
                config[c]=int(v)
            elif(k==d):
                config[d]=float(v)
            elif(k==t):
                config[t]=float(v)
    except:
        return

