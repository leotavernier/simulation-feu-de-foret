import unittest
import IO
import ihm
import parametre

class TestFonction(unittest.TestCase):

    def test_Init_Donnee(self):

        lecture = dict()
        config = dict()
        config['ligne']=5
        config['colonne']=5
        config['duree']=10
        config['forestation']=0.5
        IO.creation_fichier('test.csv',config)
        lecture = IO.lecture_fichier('test.csv' ,lecture)
        self.assertEqual(lecture['ligne'], 5)
        self.assertEqual(lecture['colonne'], 5)
        self.assertEqual(lecture['duree'], 10)
        self.assertEqual(lecture['forestation'], 0.5)

    def test_MAJ_arguments(self):
        lecture = dict()
        config = dict()
        config['ligne']=5
        config['colonne']=5
        config['duree']=10
        config['forestation']=0.5
        IO.creation_fichier('test.csv',config)
        lecture = IO.lecture_fichier('test.csv' ,lecture)
        self.assertEqual(lecture['ligne'], 5)
        self.assertEqual(lecture['colonne'], 5)
        self.assertEqual(lecture['duree'], 10)
        self.assertEqual(lecture['forestation'], 0.5)
        args = ['ligne=15','colonne=20','duree=15','forestation=0.7']
        parametre.MAJ_Arguments(config,args)
        IO.creation_fichier('test.csv',config)
        lecture = IO.lecture_fichier('test.csv' ,lecture)
        self.assertEqual(lecture['ligne'], 15)
        self.assertEqual(lecture['colonne'], 20)
        self.assertEqual(lecture['duree'], 15)
        self.assertEqual(lecture['forestation'], 0.7)


    def test_calcul_taille_cellule(self):
        self.assertEqual(ihm.calcul_taille_cellule(10,10), 80)
        self.assertEqual(ihm.calcul_taille_cellule(20,5), 40)
        self.assertEqual(ihm.calcul_taille_cellule(10,50), 16)

    def test_Creation_map(self):
        config = dict()
        config = dict()
        config['ligne']=10
        config['colonne']=10
        config['duree']=15
        config['forestation']=0.9
        config=IO.lecture_fichier('test.csv',config)
        ihm.creation_map(config)

