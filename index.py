import parametre
import ihm
import tkinter
import IO
import sys

def main(*args):

    # INITIALISATION: DONNEES
    config = dict()
    config['ligne']=5
    config['colonne']=5
    config['duree']=10
    config['forestation']=0.5
    
    # ACQUISITION: MIS A JOUR DES DONNEES EN FONCTION DES ARGUMENTS SAISIES AU SCRIPT
    parametre.MAJ_Arguments(config,args)
    IO.creation_fichier('data.csv',config)
    
    #TRAITEMENT (-> Lancement de l'IHM)
    ihm.creation_map(config)

    
if __name__ == "__main__":
    if len(sys.argv) > 5:
        raise SyntaxError("Too many arguments entered")
    else:
        # If there are keyword arguments
        main(*sys.argv[1:])
