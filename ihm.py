from tkinter import *
import datetime
from math import sqrt
from timeit import default_timer
import time
import random
import index
import IO
import sys

# Maj des labels quand on presse le bouton de reintilisation de la map
def maj_labels(cfg):
    liste_var_labels=[lbl_ligne,lbl_colonne,lbl_duree,lbl_taux_boisement]
    i=0
    for key,value in cfg.items():
        try:
            liste_var_labels[i].set(key+' : '+str(value))
        except:
            return
        i=i+1

# Procédure liée au bouton OK permettant la mise à jour du paramètre saisie.
def maj_information(l_valeur):
    list_config=list()
    for key,value in (config.items()):
        list_config.append(value)
    for index in range(4):
        try:
            if (l_valeur[index].get()>=0.0):
                list_config[index]=l_valeur[index].get()
        except Exception:
            print("Erreur de format sur la saisie des paramètres")
        IO.ecriture_fichier('data.csv',list_config)

    return list_config    

# Création des labels (juste a gauche des entry là)
def creation_labels(cfg):
    liste_valeurs_labels=list()
    liste_var_labels=[lbl_ligne,lbl_colonne,lbl_duree,lbl_taux_boisement]

    for key,value in cfg.items():
        liste_valeurs_labels.append(str(key+' : '+str(value)))

    for i in range (4):
        liste_var_labels[i].set(liste_valeurs_labels[i])
        lbl=Label(root, textvariable=liste_var_labels[i])
        lbl.grid(column=0, row=i)

# Entry avec les boutons OK
def creation_saisies():
    for i in range (4):
        entry=Entry(root,            
                textvariable=liste_valeurs[i],
                width=7)
        liste_valeurs[i].get()
        entry.grid(column=1,row=i)

# Choix de la règle
def choix_regle():
    NotImplemented

# Permet de placer des foyers d'incendie avec un clic sur une case
def callback_click(event):
    x, y = event.x, event.y
    tag = canvas.find_closest(x,y)
    if canvas.itemcget(tag, 'fill') == "green":
        canvas.itemconfigure(tag, fill="red")

# Procédure liée au bouton de reinitialisation de la map
def reinitialiser_map():
    list_config=maj_information(liste_valeurs)
    config=IO.lecture_fichier('data.csv',list_config)
    canvas.delete('all')
    creation_map(config)
    

# calcul la taille des cellules dans la zone du canvas
def calcul_taille_cellule(p_ligne,p_colonne):
    if (p_ligne < p_colonne):
        n=p_colonne*p_colonne
    elif (p_colonne < p_ligne): 
        n=p_ligne*p_ligne
    else :
        n = p_ligne*p_colonne
    taille_cellule = sqrt((hauteur*largeur)/(n))
    return taille_cellule


#animation du feu de forêt
def animation():
    while(valeur_duree.get()>0):
        time.sleep(1.000)
        propagation_feu()
        valeur_duree.set(valeur_duree.get()-1)
        root.update()
        

#parcours des rectangles du canvas
def propagation_feu():
    if(regle.get()==0):
        propagation_simple()
    elif(regle.get()==1):    
        propagation_complexe()

#propagation du feu simple (chaque case autour d'une autre case en feu prends feu)
def propagation_simple():
    for case in liste_rectangle:
        if(canvas.itemcget(case,'fill')=='red'):
            if canvas.itemcget(case+1,'fill')=='green' and case%valeur_colonne.get()!=0 :
                canvas.itemconfigure(case+1,fill='orange')
            if canvas.itemcget(case-1,'fill')=='green' and case-1%valeur_colonne.get()!=0 :
                canvas.itemconfigure(case-1,fill='orange')
            if canvas.itemcget(case+valeur_ligne.get(),'fill')=='green' :
                canvas.itemconfigure(case+valeur_ligne.get(),fill='orange')
            if canvas.itemcget(case-valeur_ligne.get(),'fill')=='green' :
                canvas.itemconfigure(case-valeur_ligne.get(),fill='orange')
    
    maj_carte()

#propagation du feu complexe (chaque case autour d'une autre case en feu à 1-(1/k+1) chance de prendre feu où k = nb de voisins)
def propagation_complexe():
    for case in liste_rectangle:
        nb_voisin=0
        if(canvas.itemcget(case,'fill')=='green'):
            if canvas.itemcget(case+1,'fill')=='red' and case%valeur_colonne.get()!=0 :
                nb_voisin=nb_voisin+1
            if canvas.itemcget(case-1,'fill')=='red' and case-1%valeur_colonne.get()!=0 :
                nb_voisin=nb_voisin+1
            if canvas.itemcget(case+valeur_ligne.get(),'fill')=='red' :
                nb_voisin=nb_voisin+1
            if canvas.itemcget(case-valeur_ligne.get(),'fill')=='red' :
                nb_voisin=nb_voisin+1
            probabilite=1-(1/(nb_voisin+1))
            probabilites=[1-probabilite,probabilite]
            couleurs=["green","orange"]
            couleur=random.choices(couleurs, probabilites)
            canvas.itemconfigure(case,fill=couleur)
        
    maj_carte()
        

#fonction permettant de passer à l'état dit "intermediaire" entre le vert le rouge (orange) à l'état de feu (rouge)
# Mettre également les cases feu en cendre (gris) ET les cases cendre en vide (blanc)
def maj_carte():
    for case in liste_rectangle:
        if(canvas.itemcget(case,'fill')=='grey'):
            canvas.itemconfigure(case,fill='white')
        if(canvas.itemcget(case,'fill')=='red'):
            canvas.itemconfigure(case,fill='grey')
        if(canvas.itemcget(case,'fill')=='orange'):
            canvas.itemconfigure(case,fill='red')

# Prend la configuration en paramètre et créer la map
def creation_map(p_config):
    IO.lecture_fichier('data.csv',config)
    root.title("Simulation de feu de forêt")
    nb_ligne=p_config['ligne']
    nb_colonne=p_config['colonne']
    taille_cellule = calcul_taille_cellule(nb_ligne,nb_colonne)
    forestation=p_config['forestation']
    couleurs=["white","green"]
    coeffs=[1-forestation,forestation]
    maj_labels(p_config)
    index_ligne=0
    index_colonne=0
    x1=0
    y1=0

    canvas.bind('<Button-1>', callback_click)
    
    while (index_ligne<nb_ligne):
        while (index_colonne<nb_colonne):
            x2=x1+taille_cellule
            reponse=random.choices(couleurs, coeffs)
            liste_rectangle.append(canvas.create_rectangle(x1,y1,x2,y1+taille_cellule, fill = reponse))
            x1+=taille_cellule
            index_colonne=index_colonne+1
            
        index_colonne=0
        x1=0
        y1=y1+taille_cellule
        index_ligne=index_ligne+1

    root.mainloop()

# Création de la fenêtre 
root = Tk(screenName="simulation_feu_foret")
root.rowconfigure(7,weight=1)
root.columnconfigure(4,weight=1)


# Création du Canvas
hauteur = 800
largeur = 800
canvas=Canvas(root, width = largeur, height = hauteur)
canvas.grid(column = 2, row = 6, sticky='nwse')

config=dict()
config=IO.lecture_fichier('data.csv',config)
liste_rectangle=list()
lbl_ligne=StringVar()
lbl_colonne=StringVar()
lbl_duree=StringVar()
lbl_taille_cellule=StringVar()
lbl_taux_boisement=StringVar()
valeur_ligne=IntVar(root,config['ligne'])
valeur_colonne=IntVar(root,config['colonne'])
valeur_duree=DoubleVar(root,config['duree'])
valeur_taux_boisement=DoubleVar(root,config['forestation'])
liste_valeurs=[valeur_ligne,valeur_colonne,valeur_duree,valeur_taux_boisement]
creation_labels(config)
creation_saisies()

# Création du bouton de reinitialisation de la map
btn_reinitialiser = Button(root,
        text='Réinitialiser la map',
        command=reinitialiser_map)
btn_reinitialiser.grid(row=7,column=4)

# Création du bouton de choix de la règle de propagation
btn_choix = Button(root,
        text='Lancer la simulation',
        command=animation)
btn_choix.grid(row=7,column=3,sticky = 'e')

# Création du bouton de choix de la règle de propagation
btn_quitter = Button(root,
        text='Quitter',
        command=root.destroy)
btn_quitter.grid(row=7,column=2,sticky = 'e')

# Création des radio bouton pour les règles de propagations de feu
regle=IntVar()

rbt_regle1=Radiobutton(root, variable=regle, text="Propagation simple", value=0)
rbt_regle2=Radiobutton(root, variable=regle, text="Propagation complexe", value=1)

rbt_regle1.grid(row=7,column=0,sticky='w')
rbt_regle2.grid(row=7,column=1,sticky='w')